package com.nutstudio.nutbus.config;

/**
 * Created by Administrator on 2017/1/20 0020.
 */

public class LineInfo {
    public static final String LINE_TABLE_NAME="line_table_db";
    public static final String LINE_ID="line_id";
    public static final String LINE_NAME="line_name";
    public static final String START_STATION_NAME="start_station_name";
    public static final String END_STATION_NAME="end_station_name";
    public static final String START_TIME="start_time";
    public static final String END_TIME="end_time";
    public static final String LINE_VALUE="line_value";
}
