package com.nutstudio.nutbus.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.nutstudio.nutbus.R;
import com.nutstudio.nutbus.adpter.ShowLinnAdapter;
import com.nutstudio.nutbus.base.NetActivity;
import com.nutstudio.nutbus.bean.LineBean;
import com.nutstudio.nutbus.bean.LineHeaderBean;
import com.nutstudio.nutbus.config.LineInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ShowLineActivity extends NetActivity {
    private static final int getLineInfoKey=1;
    private static final int getLineTimeKey=2;
    @InjectView(R.id.list_rv)
    RecyclerView listRv;
    private ShowLinnAdapter adapter;
    private List<LineBean> lineBeanList = new ArrayList<>();
    private List<LineBean> upLineBeanList = new ArrayList<>();
    private List<LineBean> downLineBeanList = new ArrayList<>();
    private LineHeaderBean headerBean=new LineHeaderBean();
    private String lineId;
    private String lineName;
    private Intent retureIntent=new Intent();
    private boolean isUpLine=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_line);
        iRequest = getRequest();
        ButterKnife.inject(this);
        initData();
        initView();
        initEvent();
    }
    private void refreshIntent(LineHeaderBean headerBean){
        retureIntent.putExtra(LineInfo.LINE_ID,lineId);
        retureIntent.putExtra(LineInfo.LINE_NAME,headerBean.getLineName());
        retureIntent.putExtra(LineInfo.START_STATION_NAME,headerBean.getStartStation());
        retureIntent.putExtra(LineInfo.END_STATION_NAME,headerBean.getEndStation());
        retureIntent.putExtra(LineInfo.START_TIME,headerBean.getStartTime());
        retureIntent.putExtra(LineInfo.END_TIME,headerBean.getEndTime());
        retureIntent.putExtra(LineInfo.LINE_VALUE,headerBean.getNormalValue());
        setResult(2,retureIntent);
    }
    private void initEvent() {
        getLineInfo(lineId);
        getLineTimeInfo(lineId);
        adapter.setOnItemClickListener(new ShowLinnAdapter.OnItemClickListener() {
            @Override
            public void onHeaderViewClick(View view) {
                if (isUpLine){
                    lineBeanList.clear();
                    for (int i=0;i<downLineBeanList.size();i++){
                        lineBeanList.add(downLineBeanList.get(i));
                    }

                    String startStationName=headerBean.getStartStation();
                    headerBean.setStartStation(headerBean.getEndStation());
                    headerBean.setEndStation(startStationName);
                    isUpLine=false;
                    adapter.notifyDataSetChanged();
                }else {
                    lineBeanList.clear();
                    for (int i=0;i<upLineBeanList.size();i++){
                        lineBeanList.add(upLineBeanList.get(i));
                    }
                    String startStationName=headerBean.getStartStation();
                    headerBean.setStartStation(headerBean.getEndStation());
                    headerBean.setEndStation(startStationName);
                    isUpLine=true;
                    adapter.notifyDataSetChanged();
                }

            }

            @Override
            public void onItemClick(View view, int pos) {
                int a=0;
                for (int i=pos;i>0;i--){
                    if (i<lineBeanList.size()&&lineBeanList.get(i).getCarNum()>0){
                        a=i;
                        if ((pos-(a+1))>0){
                            Toast.makeText(ShowLineActivity.this,"还有："+(pos-(a+1))+"站到达",Toast.LENGTH_SHORT).show();
                        }else if ((pos-(a+1))==0){
                            Toast.makeText(ShowLineActivity.this,"5分钟之内到达",Toast.LENGTH_SHORT).show();
                        }
                        return;
                    }
                }


            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        });
    }
    private void getLineInfo(String lineId){
        Map<String, String> params = new HashMap<>();
        params.put("__RequestVerificationToken", "WqqBeVAHKi6GOQSrsoUkOUzIJIIUIYvMFTM2q12lGfyc_LNHizXlJymw3_ud4trYOnHyWGgHrQuLGC-kunLutK2rAL8CqVBgTVDgs84FXNo1");
        params.put("routeid", lineId);
        iRequest.getJsonObjRequestPost("http://www.xaglkp.com.cn/Bus/GetRealBusLine",getLineInfoKey,params);
    }
    private void getLineTimeInfo(String lineId){
        iRequest.getJsonObjRequest("http://www.xaglkp.com.cn/Bus/GetBusLineInfoBySegmentId?segmentId=" +
                lineId +
                "_1",getLineTimeKey);
    }
    @Override
    public void responseSuccess(JSONObject jsonObject, int whichCall) {
        Log.d("aaa",jsonObject.toString());
        if (whichCall==getLineInfoKey){
            try {
                JSONArray lineArr=jsonObject.getJSONArray("up");
                if (lineArr.length()>0){
                    upLineBeanList.clear();
                    for (int i=0;i<lineArr.length();i++){
                        JSONObject object=lineArr.getJSONObject(i);
                        LineBean lineBean=new LineBean();
                        lineBean.setCarNum(object.getInt("bus_count"));
                        lineBean.setStationName(object.getString("station_name"));
                        lineBean.setStatinId(object.getInt("station_id"));
                        upLineBeanList.add(lineBean);
                    }
                    headerBean.setStartStation(lineArr.getJSONObject(0).getString("station_name"));
                    headerBean.setEndStation(lineArr.getJSONObject(lineArr.length()-1).getString("station_name"));
                    refreshIntent(headerBean);
                }
                lineBeanList.clear();
                for (int i=0;i<upLineBeanList.size();i++){
                    lineBeanList.add(upLineBeanList.get(i));
                }
                adapter.notifyDataSetChanged();

                JSONArray lineArrDown=jsonObject.getJSONArray("down");
                if (lineArrDown.length()>0){
                    downLineBeanList.clear();
                    for (int i=0;i<lineArrDown.length();i++){
                        JSONObject object=lineArrDown.getJSONObject(i);
                        LineBean lineBean=new LineBean();
                        lineBean.setCarNum(object.getInt("bus_count"));
                        lineBean.setStationName(object.getString("station_name"));
                        lineBean.setStatinId(object.getInt("station_id"));
                        downLineBeanList.add(lineBean);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else if (whichCall==getLineTimeKey){
            try {
                headerBean.setStartTime(jsonObject.getString("FSTSENDTIME"));
                headerBean.setEndTime(jsonObject.getString("LSTSENDTIME"));
                headerBean.setNormalValue(jsonObject.getString("NORMALVALUE"));
                refreshIntent(headerBean);
                adapter.notifyItemChanged(0);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }

    private void initView() {
        adapter = new ShowLinnAdapter(this, lineBeanList,headerBean);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        listRv.setLayoutManager(layoutManager);
        listRv.setAdapter(adapter);
    }
    private void initData() {
        lineId=getIntent().getExtras().getString("id");
        lineName=getIntent().getExtras().getString("name");
        headerBean.setLineName(lineName);
    }
}
