package com.nutstudio.nutbus.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nutstudio.nutbus.config.LineInfo;

/**
 * Created by admin on 2016/1/20.
 */
public class OrderDBHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    public static final String DB_NAME = "nut_app_db";

    public static final String KEY_ID = "_id";

    public OrderDBHelper(Context context) {
        super(context,DB_NAME, null, VERSION);
    }

    public OrderDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /*
        private static final int VERSION = 1;
        private static final String SWORD="SWORD";
        //三个不同参数的构造函数
        //带全部参数的构造函数，此构造函数必不可少
        public DatabaseHelper(Context context, String name, CursorFactory factory,
                              int version) {
            super(context, name, factory, version);

        }
        //带两个参数的构造函数，调用的其实是带三个参数的构造函数
        public DatabaseHelper(Context context,String name){
            this(context,name,VERSION);
        }
        //带三个参数的构造函数，调用的是带所有参数的构造函数
        public DatabaseHelper(Context context,String name,int version){
            this(context, name,null,version);
        }*/
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + LineInfo.LINE_TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LineInfo.LINE_ID + " TEXT,"
                + LineInfo.LINE_NAME + " TEXT,"
                + LineInfo.START_STATION_NAME + " TEXT,"
                + LineInfo.END_STATION_NAME + " TEXT,"
                + LineInfo.START_TIME + " TEXT,"
                + LineInfo.END_TIME + " TEXT,"
                + LineInfo.LINE_VALUE + " TEXT)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //删除之前的数据库
        db.execSQL("DROP TABLE   IF  EXISTS "+LineInfo.LINE_TABLE_NAME );
        db.execSQL("CREATE TABLE " + LineInfo.LINE_TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LineInfo.LINE_ID + " TEXT,"
                + LineInfo.LINE_NAME + " TEXT,"
                + LineInfo.START_STATION_NAME + " TEXT,"
                + LineInfo.END_STATION_NAME + " TEXT,"
                + LineInfo.START_TIME + " TEXT,"
                + LineInfo.END_TIME + " TEXT,"
                + LineInfo.LINE_VALUE + " TEXT)");
    }
}
