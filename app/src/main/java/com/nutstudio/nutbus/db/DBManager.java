package com.nutstudio.nutbus.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.nutstudio.nutbus.bean.LineHeaderBean;
import com.nutstudio.nutbus.config.LineInfo;

import java.util.ArrayList;
import java.util.List;

public class DBManager {
    private OrderDBHelper helper;
    private SQLiteDatabase db;

    public DBManager(Context context) {
        helper = new OrderDBHelper(context);
        // 因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0,
        // mFactory);
        // 所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里
        db = helper.getWritableDatabase();
    }


    public void excute(String sql) {
        db.execSQL(sql);
        // db.close();
    }
    // 删除单个数据
    public void deleteSingle(String lineID) {
        db.execSQL("delete from " + LineInfo.LINE_TABLE_NAME + " where " + LineInfo.LINE_ID + " = '" + lineID + "'");
    }
    public List<LineHeaderBean> queryAlldata(String sql) {
        List<LineHeaderBean> orderInfos = new ArrayList<LineHeaderBean>();

        Cursor c = db.rawQuery(sql, null);
        while (c.moveToNext()) {
            LineHeaderBean orderInfo = new LineHeaderBean();
            orderInfo.setLineId(c.getString(c.getColumnIndex(LineInfo.LINE_ID)));
            orderInfo.setLineName(c.getString(c.getColumnIndex(LineInfo.LINE_NAME)));
            orderInfo.setStartStation(c.getString(c.getColumnIndex(LineInfo.START_STATION_NAME)));
            orderInfo.setEndStation(c.getString(c.getColumnIndex(LineInfo.END_STATION_NAME)));
            orderInfo.setStartTime(c.getString(c.getColumnIndex(LineInfo.START_TIME)));
            orderInfo.setEndTime(c.getString(c.getColumnIndex(LineInfo.END_TIME)));
            orderInfo.setNormalValue(c.getString(c.getColumnIndex(LineInfo.LINE_VALUE)));
            orderInfos.add(orderInfo);
        }
        c.close();
        return orderInfos;
    }
    public int updatesingle(LineHeaderBean orderInfo) {
        ContentValues cv = new ContentValues();
        cv.put(LineInfo.LINE_NAME, orderInfo.getLineName());
        cv.put(LineInfo.START_STATION_NAME, orderInfo.getStartStation());
        cv.put(LineInfo.END_STATION_NAME, orderInfo.getEndStation());
        cv.put(LineInfo.START_TIME, orderInfo.getStartTime());
        cv.put(LineInfo.END_TIME, orderInfo.getEndTime());
        cv.put(LineInfo.LINE_VALUE, orderInfo.getNormalValue());
        String[] args = {String.valueOf(orderInfo.getLineId())};
        return db.update(LineInfo.LINE_TABLE_NAME, cv, LineInfo.LINE_ID + "=?", args);
    }
    public int addsingle(LineHeaderBean orderInfo) {
        int myidx = 0;
        db.beginTransaction(); // 开始事务
        try {
            db.execSQL(
                    "INSERT INTO " + LineInfo.LINE_TABLE_NAME + "("
                            + LineInfo.LINE_ID + ","
                            + LineInfo.LINE_NAME + ","
                            + LineInfo.START_STATION_NAME + ","
                            + LineInfo.END_STATION_NAME + ","
                            + LineInfo.START_TIME + ","
                            + LineInfo.END_TIME + ","
                            + LineInfo.LINE_VALUE + ") VALUES(?,?,?,?,?,?,?)",
                    new Object[]{orderInfo.getLineId(), orderInfo.getLineName(), orderInfo.getStartStation(),
                            orderInfo.getEndStation(), orderInfo.getStartTime(), orderInfo.getEndTime(),
                            orderInfo.getNormalValue()});

            Cursor curx = db.rawQuery("select LAST_INSERT_ROWID() ", null);
            curx.moveToFirst();
            myidx = curx.getInt(0);
            db.setTransactionSuccessful(); // 设置事务成功完成
        } finally {
            db.endTransaction(); // 结束事务
        }
        return myidx;
    }

    /**
     * query all persons, return cursor
     *
     * @return Cursor
     */
    // public Cursor queryTheCursor() {
    // Cursor c = db.rawQuery("SELECT * FROM barcode", null);
    // return c;
    // }

    /**
     * close database
     */
    public void closeDB() {
        db.close();
    }
}
