package com.nutstudio.nutbus.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nutstudio.nutbus.R;
import com.nutstudio.nutbus.bean.LineHeaderBean;

import java.util.List;

/**
 * 扫描列表适配器
 * Created by admin on 2016/1/13.
 */
public class SearchListAdapter extends RecyclerView.Adapter<SearchListAdapter.ViewHolder> {
    private List<LineHeaderBean> list;
    private LayoutInflater mInflater;
    private Context context;


    public SearchListAdapter(Context context, List<LineHeaderBean> list) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);
        void onColoctionClick(View view,int pos);
        void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = mInflater.inflate(R.layout.search_list_item, parent, false);
         final ViewHolder viewHolder = new ViewHolder(view);
        if (mOnItemClickLitener!=null){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickLitener.onItemClick(view,viewHolder.getLayoutPosition());
                }
            });
            viewHolder.collectonIm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickLitener.onColoctionClick(view,viewHolder.getLayoutPosition());
                }
            });
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (position < list.size()) {
            LineHeaderBean headerBean=list.get(position);
            holder.lineName.setText(headerBean.getLineName());
            if (headerBean.isSelect()){
                holder.collectonIm.setImageResource(R.drawable.ic_star_black_24dp);
            }else {
                holder.collectonIm.setImageResource(R.drawable.ic_star_border_black_24dp);
            }
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lineName;
        private ImageView collectonIm;

        ViewHolder(View itemView) {
            super(itemView);
            lineName = (TextView) itemView.findViewById(R.id.line_name_tv);
            collectonIm = (ImageView) itemView.findViewById(R.id.collection_im);
        }
    }
}
