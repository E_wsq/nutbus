package com.nutstudio.nutbus.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nutstudio.nutbus.R;
import com.nutstudio.nutbus.bean.LineHeaderBean;

import java.util.List;

/**
 * 扫描列表适配器
 * Created by admin on 2016/1/13.
 */
public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {
    private List<LineHeaderBean> list;
    private LayoutInflater mInflater;
    private Context context;


    public MainListAdapter(Context context, List<LineHeaderBean> list) {
        mInflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
    }

    public interface OnItemClickLitener {
        void onItemClick(View view, int position);

        void onItemLongClick(View view, int position);
    }

    private OnItemClickLitener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickLitener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }

    @Override
    public ViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View view = mInflater.inflate(R.layout.main_list_item, parent, false);
         final ViewHolder viewHolder = new ViewHolder(view);
        if (mOnItemClickLitener!=null){
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mOnItemClickLitener.onItemClick(view,viewHolder.getLayoutPosition());
                }
            });
            view.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    mOnItemClickLitener.onItemLongClick(view,viewHolder.getLayoutPosition());
                    return false;
                }
            });
        }
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        if (position < list.size()) {
            LineHeaderBean headerBean=list.get(position);
            if (headerBean.getStartStation()!=null&&headerBean.getStartStation().length()>0){
                holder.startEndStationTv.setVisibility(View.VISIBLE);
                holder.startEndStationTv.setText(headerBean.getStartStation()+"→"+headerBean.getEndStation());
            }else {
                holder.startEndStationTv.setVisibility(View.GONE);
            }
            if (headerBean.getStartTime()!=null&&headerBean.getStartTime().length()>0){
                holder.startTiemTv.setVisibility(View.VISIBLE);
                holder.startTiemTv.setText("首班："+headerBean.getStartTime().substring(headerBean.getStartTime().indexOf(" ")));
            }else {
                holder.startTiemTv.setVisibility(View.GONE);
            }
            if (headerBean.getEndTime()!=null&&headerBean.getEndTime().length()>0){
                holder.endTimeTv.setVisibility(View.VISIBLE);
                holder.endTimeTv.setText("末班："+headerBean.getEndTime().substring(headerBean.getEndTime().indexOf(" ")));
            }else {
                holder.endTimeTv.setVisibility(View.GONE);
            }
            if (headerBean.getNormalValue()!=null&&headerBean.getNormalValue().length()>0){
                holder.valueTv.setVisibility(View.VISIBLE);
                holder.valueTv.setText("票价："+headerBean.getNormalValue()+"元");
            }else {
                holder.valueTv.setVisibility(View.GONE);
            }
            holder.lineNameTv.setText(headerBean.getLineName());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView startEndStationTv;
        private TextView startTiemTv;
        private TextView endTimeTv;
        private TextView valueTv;
        private TextView lineNameTv;

        ViewHolder(View itemView) {
            super(itemView);
            startEndStationTv = (TextView) itemView.findViewById(R.id.start_end_station_tv);
            startTiemTv = (TextView) itemView.findViewById(R.id.startTime_vt);
            endTimeTv = (TextView) itemView.findViewById(R.id.endTime_tv);
            valueTv = (TextView) itemView.findViewById(R.id.value_tv);
            lineNameTv = (TextView) itemView.findViewById(R.id.line_name_tv);
        }
    }
}
