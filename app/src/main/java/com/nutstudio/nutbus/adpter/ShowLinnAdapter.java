package com.nutstudio.nutbus.adpter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.nutstudio.nutbus.R;
import com.nutstudio.nutbus.bean.LineBean;
import com.nutstudio.nutbus.bean.LineHeaderBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * Created by admin on 2016/4/8.
 */
public class ShowLinnAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    //type
    private static final int TYPE_HEADER = 1;
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_FOOTER = 2;

    public static final int LOADING = 1;
    public static final int ERROR = 2;
    public static final int EMPTY_DATA = 3;
    private boolean isAddBookList = false;
    private int loadMoreStatus;

    private Context context;
    private List<LineBean> lineBeanList = new ArrayList<>();
    private LineHeaderBean headerBean=new LineHeaderBean();

    public ShowLinnAdapter(Context context, List<LineBean> lineBeanList,LineHeaderBean headerBean) {
        this.context = context;
        this.lineBeanList = lineBeanList;
        this.headerBean=headerBean;
    }

    public int getLoadMoreStatus() {
        return loadMoreStatus;
    }

    public void setLoadMoreStatus(int loadMoreStatus) {
        this.loadMoreStatus = loadMoreStatus;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }else {
            return TYPE_ITEM;
        }
    }

    public boolean isAddBookList() {
        return isAddBookList;
    }

    public void setAddBookList(boolean addBookList) {
        isAddBookList = addBookList;
    }

    public interface OnItemClickListener {
        void onHeaderViewClick(View view);
        void onItemClick(View view, int pos);

        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            return new ItemMyViewHolder(LayoutInflater.from(
                    context).inflate(R.layout.line_item, parent,
                    false));
        } else if (viewType == TYPE_HEADER) {
            return new HeaderViewHolder(LayoutInflater.from(
                    context).inflate(R.layout.line_item_header, parent,
                    false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemMyViewHolder) {
            position=position-1;
            if (position>=0&&position<lineBeanList.size()){
                LineBean lineBean=lineBeanList.get(position);
                if (lineBean.getCarNum()>0){
                    ((ItemMyViewHolder) holder).stationImg.setBackgroundResource(R.drawable.round);
                    ((ItemMyViewHolder) holder).leftTv.setVisibility(View.VISIBLE);
                    ((ItemMyViewHolder) holder).leftTv.setText(lineBean.getCarNum()+"");
                }else {
                    ((ItemMyViewHolder) holder).stationImg.setBackgroundResource(R.drawable.round_white);
                    ((ItemMyViewHolder) holder).leftTv.setVisibility(View.GONE);
                }
                ((ItemMyViewHolder) holder).stationNameTv.setText(lineBean.getStationName());

            }
            if (mOnItemClickListener!=null){
                ((ItemMyViewHolder) holder).stationNameTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickListener.onItemClick(v, holder.getLayoutPosition());
                    }
                });
            }
        } else if (holder instanceof HeaderViewHolder) {
            if (headerBean.getStartStation()!=null&&headerBean.getStartStation().length()>0){
                ((HeaderViewHolder) holder).startEndStationTv.setVisibility(View.VISIBLE);
                ((HeaderViewHolder) holder).startEndStationTv.setText(headerBean.getStartStation()+"→"+headerBean.getEndStation());
            }else {
                ((HeaderViewHolder) holder).startEndStationTv.setVisibility(View.GONE);
            }
           if (headerBean.getStartTime()!=null&&headerBean.getStartTime().length()>0){
               ((HeaderViewHolder) holder).startTiemTv.setVisibility(View.VISIBLE);
               ((HeaderViewHolder) holder).startTiemTv.setText("首班："+headerBean.getStartTime().substring(headerBean.getStartTime().indexOf(" ")));
           }else {
               ((HeaderViewHolder) holder).startTiemTv.setVisibility(View.GONE);
           }
            if (headerBean.getEndTime()!=null&&headerBean.getEndTime().length()>0){
                ((HeaderViewHolder) holder).endTimeTv.setVisibility(View.VISIBLE);
                ((HeaderViewHolder) holder).endTimeTv.setText("末班："+headerBean.getEndTime().substring(headerBean.getEndTime().indexOf(" ")));
            }else {
                ((HeaderViewHolder) holder).endTimeTv.setVisibility(View.GONE);
            }
            if (headerBean.getNormalValue()!=null&&headerBean.getNormalValue().length()>0){
                ((HeaderViewHolder) holder).valueTv.setVisibility(View.VISIBLE);
                ((HeaderViewHolder) holder).valueTv.setText("票价："+headerBean.getNormalValue()+"元");
            }else {
                ((HeaderViewHolder) holder).valueTv.setVisibility(View.GONE);
            }

            ((HeaderViewHolder) holder).linaNameTv.setText(headerBean.getLineName());
            if (mOnItemClickListener!=null){
                ((HeaderViewHolder) holder).startEndStationTv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mOnItemClickListener.onHeaderViewClick(v);
                    }
                });
            }
        }
    }



    @Override
    public int getItemCount() {
        return lineBeanList.size()+1;
    }

   private class HeaderViewHolder extends RecyclerView.ViewHolder {

        //label
        private TextView startEndStationTv;
       private TextView startTiemTv;
       private TextView endTimeTv;
       private TextView valueTv;
       private TextView linaNameTv;

        private HeaderViewHolder(View itemView) {
            super(itemView);
            startEndStationTv = (TextView) itemView.findViewById(R.id.start_end_station_tv);
            startTiemTv = (TextView) itemView.findViewById(R.id.startTime_vt);
            endTimeTv = (TextView) itemView.findViewById(R.id.endTime_tv);
            valueTv = (TextView) itemView.findViewById(R.id.value_tv);
            linaNameTv = (TextView) itemView.findViewById(R.id.line_name_tv);
        }
    }

    class ItemMyViewHolder extends RecyclerView.ViewHolder {
        private ImageView stationImg;
        private TextView leftTv;
        private TextView stationNameTv;


        private ItemMyViewHolder(View view) {
            super(view);
            stationImg = (ImageView) view.findViewById(R.id.image);
            leftTv=(TextView)view.findViewById(R.id.left_tv);
            stationNameTv=(TextView)view.findViewById(R.id.station_name_tv);
        }
    }
}
