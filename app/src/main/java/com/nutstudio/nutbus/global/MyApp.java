package com.nutstudio.nutbus.global;

import android.app.Application;
import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * crash nullpointerexception TextArea.drawout >>TextArea.java 229
 * customtostt only the original thread that created
 * Created by Administrator on 2016/11/18 0018.
 */

public class MyApp extends Application {
    private static RequestQueue requestQueue;
    private static Context context;
    @Override
    public void onCreate() {
        super.onCreate();
        requestQueue = Volley.newRequestQueue(getApplicationContext());
        context=getApplicationContext();
    }
    @Override
    public void onTerminate() {
        super.onTerminate();
        requestQueue.stop();
    }
    public static RequestQueue getHttpQueue() {
        return requestQueue;
    }
    public static Context getContext(){
        return context;
    }
}
