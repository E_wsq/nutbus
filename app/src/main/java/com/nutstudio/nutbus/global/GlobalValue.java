package com.nutstudio.nutbus.global;

/**全局临时变量
 * Created by Administrator on 2016/11/18 0018.
 */

public class GlobalValue {
   //http://www.xajtfb.cn/Bus/GetBusLineByName?buslinename=1

    public static String searchLine="http://www.xajtfb.cn/Bus/GetBusLineByName?buslinename=";
    /**
     *
     * lineId	2956913890
     direction	0
     stationName	0760076
     nextStationName	K18
     s	h5
     v	3.1.12
     src	webapp_xiantraffic
     userId	browser_1488366025106
     h5Id	browser_1488366025106
     sign	1
     cityId	076
     http://www.xajtfb.cn/api/bus/line!lineDetail.action?lineId=2956913890&direction=0&stationName=0760076&nextStationName=K18&s=h5&v=3.1.12&src=webapp_xiantraffic&userId=browser_1488366025106&h5Id=browser_1488366025106&sign=1&cityId=076
     */
    public static String getLineInfo="http://www.xajtfb.cn/api/bus/line!lineDetail.action?" +
            "lineId=" +
            "2956913890" +
            "&direction=" +
            "0" +
            "&stationName=" +
            "0760076" +
            "&nextStationName=" +
            "K18" +
            "&s=" +
            "h5" +
            "&v=" +
            "3.1.12" +
            "&src=" +
            "webapp_xiantraffic" +
            "&userId=" +
            "browser_1488366025106" +
            "&h5Id=" +
            "browser_1488366025106" +
            "&sign=" +
            "1" +
            "&cityId=" +
            "076";
    public static String getTargetStationinfo="http://www.xajtfb.cn/api/bus/line!lineDetail.action?" +
            "lineId=" +
            "2956913890" +
            "&lineName=" +
            "K18" +
            "&direction=1" +
            "&stationName=" +
            "%E6%98%8E%E5%BE%B7%E9%97%A8" +
            "&nextStationName=" +
            "%E5%8D%97%E9%83%8A%E5%82%A8%E5%A4%87%E5%8E%82" +
            "&targetOrder=" +
            "31" +
            "&s=" +
            "h5" +
            "&v=" +
            "3.1.12" +
            "&src=webapp_xiantraffic" +
            "&userId=" +
            "browser_1488366025106" +
            "&h5Id=" +
            "browser_1488366025106" +
            "&sign=1" +
            "&cityId=" +
            "076";
}
